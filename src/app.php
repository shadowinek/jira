<?php

    require_once(realpath(__DIR__ . '/../vendor/autoload.php'));
    require_once(__DIR__ . '/config.php');
    require_once(__DIR__ . '/TimeTrack.php');

    $app = new Silex\Application();

    $tt = new TimeTrack($host, $user, $pass, $project, $users);

    $app->register(new Silex\Provider\TwigServiceProvider(), array(
        'twig.path'        => 'views',
        'twig.class_path'  => 'vendor/Twig/lib'
    ));

    /**
     * Blank page
     */
    $app->get('/', function() use($app) {
        return $app['twig']->render('index.twig', array());
    });

    /**
     * Report page for given time
     */
    $app->get('/{date}', function($date) use($app, $tt, $timeLimit) {
        $timeTrack = $tt->parseTime($app->escape($date));

        return $app['twig']->render('index.twig', array(
                'timeTrack' => $timeTrack,
                'timeLimit' => $timeLimit,
                'date' => $date
            )
        );
    });

    $app->run();