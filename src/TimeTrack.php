<?php
    class TimeTrack {

        /**
         * Jira URL
         */
        private $host;

        /**
         * Username for JIRA
         * @var string
         */
        private $user;

        /**
         * Password for JIRA
         * @var string
         */
        private $pass;

        /**
         * Project name
         * @var string
         */
        private $project;

        /**
         * Array of tracked users
         * @var array
         */
        private $users;

        /**
         * @var boolean
         */
        private $usersDefined = false;

        /**
         * Curl object
         * @var Curl
         */
        private $curl;

        /**
         * Array for time tracking results
         * @var array
         */
        private $time = array();

        /**
         * Starting date - format YYYY-MM-DD (e.g. 2014-03-17)
         * @var string
         */
        private $date;

        /**
         * Start date
         * @var DateTime
         */
        private $startDate;

        /**
         * End date
         * @var DateTime
         */
        private $endDate;

        /**
         * Constructor
         *
         * @param string $host
         * @param string $user
         * @param string $pass
         * @param string $project
         * @param string $users
         */
        public function __construct($host, $user, $pass, $project, $users = array()) {
            $this->host = $host;
            $this->user = $user;
            $this->pass = $pass;
            $this->project = $project;
            $this->users = $users;

            $this->loadUsers();
            $this->initCurl();
        }

        /**
         * Init Curl object
         */
        private function initCurl() {
            $this->curl = new Curl();
            $this->curl->setBasicAuthentication($this->user, $this->pass);
            $this->curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
        }

        /**
         * Load users from array and set $usersDefined flag
         */
        private function loadUsers() {
            if (!empty($this->users)) {
                $this->usersDefined = true;

                foreach ($this->users as $user) {
                    $this->time[$user]['time'] = 0;
                    $this->time[$user]['issues'] = array();
                }
            }
        }

        /**
         * Parse time tracking for given interval
         *
         * @param string $date
         * @param string $interval - String for DateInterval
         * @return array
         */
        public function parseTime($date, $interval = "P7D") {
            $this->date = $date;
            $this->startDate = new DateTime($date);
            $this->endDate = new DateTime($date);
            $this->endDate->add(new DateInterval($interval));

            $this->parseIssues();

            return $this->time;
        }

        /**
         * Download issues
         *
         * @todo - add support for multiple projects
         */
        private function parseIssues() {
            $issuesQuery = "{$this->host}rest/api/2/search?startAt=%d&jql=project={$this->project}+and+updatedDate+>=+{$this->date}"; // maxResult=10&
            $continue = true;
            $start = 0;

            while ($continue) {
                $this->curl->get(sprintf($issuesQuery, $start), array());

                if ($this->curl->error) {
                    var_dump($this->curl->error_message);
                } else {
                    $issues = json_decode($this->curl->response);
                }

                $this->parseLogs($issues->issues);

                $start += 50;

                if ($start >= $issues->total) {
                    $continue = false;
                }
            }
        }

        /**
         * Download time tracking logs for given issues
         * @param type $issues
         */
        private function parseLogs($issues) {
            $worklogQuery = "{$this->host}rest/api/2/issue/%s/worklog/";

            foreach ($issues as $issue) {

                if (!empty($issue->key)) {

                    $this->curl->get(sprintf($worklogQuery, $issue->key));

                    if ($this->curl->error) {
                        var_dump($this->curl->error_message);
                    } else {
                        $logs = json_decode($this->curl->response);
                    }

                    if (!empty($logs->worklogs)) {
                        $this->parseTimeFromLogs($logs->worklogs, $issue->key);
                    }
                }
            }
        }

        /**
         * Parse given time tracking log
         * @param array $logs
         * @param string $key
         */
        private function parseTimeFromLogs($logs, $key) {

            foreach ($logs as $log) {
                $time = new DateTime($log->updated);

                // Correct time
                if ($time >= $this->startDate && $time < $this->endDate) {

                    // Defined users
                    if ($this->usersDefined) {
                        if (isset($this->time[$log->author->name])) {
                            $this->logTime($log, $key);
                        }
                    // All users
                    } else {
                        if (!isset($this->time[$log->author->name])) {
                            $this->time[$log->author->name]["time"] = 0;
                            $this->time[$log->author->name]["issues"] = array();
                        }

                        $this->logTime($log, $key);
                    }
                }
            }
        }

        /**
         * Log given time
         * @param string $log
         * @param string $key
         */
        private function logTime($log, $key) {
            $this->time[$log->author->name]['time'] += $log->timeSpentSeconds/3600;
            $this->time[$log->author->name]['issues'][$key] = $key;
        }
    }